<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAccessAPIRequest;
use App\Http\Requests\API\UpdateAccessAPIRequest;
use App\Models\Access;
use App\Repositories\AccessRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AccessController
 * @package App\Http\Controllers\API
 */

class AccessAPIController extends AppBaseController
{
    /** @var  AccessRepository */
    private $accessRepository;

    public function __construct(AccessRepository $accessRepo)
    {
        $this->accessRepository = $accessRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/accesses",
     *      summary="Get a listing of the Accesses.",
     *      tags={"Access"},
     *      description="Get all Accesses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Access")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $accesses = $this->accessRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $rolsTotal = $this->accessRepository->all()->count();
        return $this->sendResponsePages($accesses->toArray(), 'Accesses retrieved successfully', $rolsTotal);
    }

    /**
     * @param CreateAccessAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/accesses",
     *      summary="Store a newly created Access in storage",
     *      tags={"Access"},
     *      description="Store Access",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Access that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Access")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Access"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAccessAPIRequest $request)
    {
        $input = $request->all();

        $access = $this->accessRepository->create($input);

        return $this->sendResponse($access->toArray(), 'Access saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/accesses/{id}",
     *      summary="Display the specified Access",
     *      tags={"Access"},
     *      description="Get Access",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Access",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Access"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Access $access */
        $access = $this->accessRepository->find($id);

        if (empty($access)) {
            return $this->sendError('Access not found');
        }

        return $this->sendResponse($access->toArray(), 'Access retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAccessAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/accesses/{id}",
     *      summary="Update the specified Access in storage",
     *      tags={"Access"},
     *      description="Update Access",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Access",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Access that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Access")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Access"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAccessAPIRequest $request)
    {
        $input = $request->all();

        /** @var Access $access */
        $access = $this->accessRepository->find($id);

        if (empty($access)) {
            return $this->sendError('Access not found');
        }

        $access = $this->accessRepository->update($input, $id);

        return $this->sendResponse($access->toArray(), 'Access updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/accesses/{id}",
     *      summary="Remove the specified Access from storage",
     *      tags={"Access"},
     *      description="Delete Access",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Access",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Access $access */
        $access = $this->accessRepository->find($id);

        if (empty($access)) {
            return $this->sendError('Access not found');
        }

        $access->delete();

        return $this->sendSuccess('Access deleted successfully');
    }
}
