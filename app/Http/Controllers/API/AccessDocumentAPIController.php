<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAccessDocumentAPIRequest;
use App\Http\Requests\API\UpdateAccessDocumentAPIRequest;
use App\Models\AccessDocument;
use App\Repositories\AccessDocumentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AccessDocumentController
 * @package App\Http\Controllers\API
 */

class AccessDocumentAPIController extends AppBaseController
{
    /** @var  AccessDocumentRepository */
    private $accessDocumentRepository;

    public function __construct(AccessDocumentRepository $accessDocumentRepo)
    {
        $this->accessDocumentRepository = $accessDocumentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/accessDocuments",
     *      summary="Get a listing of the AccessDocuments.",
     *      tags={"AccessDocument"},
     *      description="Get all AccessDocuments",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/AccessDocument")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $accessDocuments = $this->accessDocumentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($accessDocuments->toArray(), 'Access Documents retrieved successfully');
    }

    /**
     * @param CreateAccessDocumentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/accessDocuments",
     *      summary="Store a newly created AccessDocument in storage",
     *      tags={"AccessDocument"},
     *      description="Store AccessDocument",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AccessDocument that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AccessDocument")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AccessDocument"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAccessDocumentAPIRequest $request)
    {
        $input = $request->all();

        $accessDocument = $this->accessDocumentRepository->create($input);

        return $this->sendResponse($accessDocument->toArray(), 'Access Document saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/accessDocuments/{id}",
     *      summary="Display the specified AccessDocument",
     *      tags={"AccessDocument"},
     *      description="Get AccessDocument",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AccessDocument",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AccessDocument"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var AccessDocument $accessDocument */
        $accessDocument = $this->accessDocumentRepository->find($id);

        if (empty($accessDocument)) {
            return $this->sendError('Access Document not found');
        }

        return $this->sendResponse($accessDocument->toArray(), 'Access Document retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAccessDocumentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/accessDocuments/{id}",
     *      summary="Update the specified AccessDocument in storage",
     *      tags={"AccessDocument"},
     *      description="Update AccessDocument",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AccessDocument",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AccessDocument that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AccessDocument")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AccessDocument"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAccessDocumentAPIRequest $request)
    {
        $input = $request->all();

        /** @var AccessDocument $accessDocument */
        $accessDocument = $this->accessDocumentRepository->find($id);

        if (empty($accessDocument)) {
            return $this->sendError('Access Document not found');
        }

        $accessDocument = $this->accessDocumentRepository->update($input, $id);

        return $this->sendResponse($accessDocument->toArray(), 'AccessDocument updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/accessDocuments/{id}",
     *      summary="Remove the specified AccessDocument from storage",
     *      tags={"AccessDocument"},
     *      description="Delete AccessDocument",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AccessDocument",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var AccessDocument $accessDocument */
        $accessDocument = $this->accessDocumentRepository->find($id);

        if (empty($accessDocument)) {
            return $this->sendError('Access Document not found');
        }

        $accessDocument->delete();

        return $this->sendSuccess('Access Document deleted successfully');
    }
}
