<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDocumentsAPIRequest;
use App\Http\Requests\API\UpdateDocumentsAPIRequest;
use App\Models\Documents;
use App\Repositories\DocumentsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DocumentsController
 * @package App\Http\Controllers\API
 */

class DocumentsAPIController extends AppBaseController
{
    /** @var  DocumentsRepository */
    private $documentsRepository;

    public function __construct(DocumentsRepository $documentsRepo)
    {
        $this->documentsRepository = $documentsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/documents",
     *      summary="Get a listing of the Documents.",
     *      tags={"Documents"},
     *      description="Get all Documents",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Documents")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $documents = $this->documentsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($documents->toArray(), 'Documents retrieved successfully');
    }

    /**
     * @param CreateDocumentsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/documents",
     *      summary="Store a newly created Documents in storage",
     *      tags={"Documents"},
     *      description="Store Documents",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Documents that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Documents")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Documents"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDocumentsAPIRequest $request)
    {
        $input = $request->all();

        $documents = $this->documentsRepository->create($input);

        return $this->sendResponse($documents->toArray(), 'Documents saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/documents/{id}",
     *      summary="Display the specified Documents",
     *      tags={"Documents"},
     *      description="Get Documents",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Documents",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Documents"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Documents $documents */
        $documents = $this->documentsRepository->find($id);

        if (empty($documents)) {
            return $this->sendError('Documents not found');
        }

        return $this->sendResponse($documents->toArray(), 'Documents retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDocumentsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/documents/{id}",
     *      summary="Update the specified Documents in storage",
     *      tags={"Documents"},
     *      description="Update Documents",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Documents",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Documents that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Documents")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Documents"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDocumentsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Documents $documents */
        $documents = $this->documentsRepository->find($id);

        if (empty($documents)) {
            return $this->sendError('Documents not found');
        }

        $documents = $this->documentsRepository->update($input, $id);

        return $this->sendResponse($documents->toArray(), 'Documents updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/documents/{id}",
     *      summary="Remove the specified Documents from storage",
     *      tags={"Documents"},
     *      description="Delete Documents",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Documents",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Documents $documents */
        $documents = $this->documentsRepository->find($id);

        if (empty($documents)) {
            return $this->sendError('Documents not found');
        }

        $documents->delete();

        return $this->sendSuccess('Documents deleted successfully');
    }
}
