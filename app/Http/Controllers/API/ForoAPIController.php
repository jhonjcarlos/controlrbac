<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateForoAPIRequest;
use App\Http\Requests\API\UpdateForoAPIRequest;
use App\Models\Foro;
use App\Repositories\ForoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ForoController
 * @package App\Http\Controllers\API
 */

class ForoAPIController extends AppBaseController
{
    /** @var  ForoRepository */
    private $foroRepository;

    public function __construct(ForoRepository $foroRepo)
    {
        $this->foroRepository = $foroRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/foros",
     *      summary="Get a listing of the Foros.",
     *      tags={"Foro"},
     *      description="Get all Foros",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Foro")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $foros = $this->foroRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($foros->toArray(), 'Foros retrieved successfully');
    }

    /**
     * @param CreateForoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/foros",
     *      summary="Store a newly created Foro in storage",
     *      tags={"Foro"},
     *      description="Store Foro",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Foro that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Foro")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Foro"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateForoAPIRequest $request)
    {
        $input = $request->all();

        $foro = $this->foroRepository->create($input);

        return $this->sendResponse($foro->toArray(), 'Foro saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/foros/{id}",
     *      summary="Display the specified Foro",
     *      tags={"Foro"},
     *      description="Get Foro",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Foro",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Foro"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Foro $foro */
        $foro = $this->foroRepository->find($id);

        if (empty($foro)) {
            return $this->sendError('Foro not found');
        }

        return $this->sendResponse($foro->toArray(), 'Foro retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateForoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/foros/{id}",
     *      summary="Update the specified Foro in storage",
     *      tags={"Foro"},
     *      description="Update Foro",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Foro",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Foro that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Foro")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Foro"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateForoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Foro $foro */
        $foro = $this->foroRepository->find($id);

        if (empty($foro)) {
            return $this->sendError('Foro not found');
        }

        $foro = $this->foroRepository->update($input, $id);

        return $this->sendResponse($foro->toArray(), 'Foro updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/foros/{id}",
     *      summary="Remove the specified Foro from storage",
     *      tags={"Foro"},
     *      description="Delete Foro",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Foro",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Foro $foro */
        $foro = $this->foroRepository->find($id);

        if (empty($foro)) {
            return $this->sendError('Foro not found');
        }

        $foro->delete();

        return $this->sendSuccess('Foro deleted successfully');
    }
}
