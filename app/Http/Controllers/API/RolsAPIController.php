<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRolsAPIRequest;
use App\Http\Requests\API\UpdateRolsAPIRequest;
use App\Models\Rols;
use App\Repositories\RolsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RolsController
 * @package App\Http\Controllers\API
 */

class RolsAPIController extends AppBaseController
{
    /** @var  RolsRepository */
    private $rolsRepository;

    public function __construct(RolsRepository $rolsRepo)
    {
        $this->rolsRepository = $rolsRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/rols",
     *      summary="Get a listing of the Rols.",
     *      tags={"Rols"},
     *      description="Get all Rols",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Rols")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $rols = $this->rolsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $rolsTotal = $this->rolsRepository->all()->count();
        return $this->sendResponsePages($rols->toArray(), 'Rols retrieved successfully', $rolsTotal);
    }

    /**
     * @param CreateRolsAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/rols",
     *      summary="Store a newly created Rols in storage",
     *      tags={"Rols"},
     *      description="Store Rols",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Rols that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Rols")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rols"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRolsAPIRequest $request)
    {
        $input = $request->all();

        $rols = $this->rolsRepository->create($input);

        return $this->sendResponse($rols->toArray(), 'Rols saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/rols/{id}",
     *      summary="Display the specified Rols",
     *      tags={"Rols"},
     *      description="Get Rols",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rols",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rols"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Rols $rols */
        $rols = $this->rolsRepository->find($id);

        if (empty($rols)) {
            return $this->sendError('Rols not found');
        }

        return $this->sendResponse($rols->toArray(), 'Rols retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRolsAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/rols/{id}",
     *      summary="Update the specified Rols in storage",
     *      tags={"Rols"},
     *      description="Update Rols",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rols",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Rols that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Rols")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Rols"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRolsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Rols $rols */
        $rols = $this->rolsRepository->find($id);

        if (empty($rols)) {
            return $this->sendError('Rols not found');
        }

        $rols = $this->rolsRepository->update($input, $id);

        return $this->sendResponse($rols->toArray(), 'Rols updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/rols/{id}",
     *      summary="Remove the specified Rols from storage",
     *      tags={"Rols"},
     *      description="Delete Rols",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Rols",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Rols $rols */
        $rols = $this->rolsRepository->find($id);

        if (empty($rols)) {
            return $this->sendError('Rols not found');
        }

        $rols->delete();

        return $this->sendSuccess('Rols deleted successfully');
    }
}
