<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeDocumentAPIRequest;
use App\Http\Requests\API\UpdateTypeDocumentAPIRequest;
use App\Models\TypeDocument;
use App\Repositories\TypeDocumentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TypeDocumentController
 * @package App\Http\Controllers\API
 */

class TypeDocumentAPIController extends AppBaseController
{
    /** @var  TypeDocumentRepository */
    private $typeDocumentRepository;

    public function __construct(TypeDocumentRepository $typeDocumentRepo)
    {
        $this->typeDocumentRepository = $typeDocumentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/typeDocuments",
     *      summary="Get a listing of the TypeDocuments.",
     *      tags={"TypeDocument"},
     *      description="Get all TypeDocuments",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TypeDocument")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $typeDocuments = $this->typeDocumentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $rolsTotal = $this->typeDocumentRepository->all()->count();
        return $this->sendResponsePages($typeDocuments->toArray(), 'Type Documents retrieved successfully', $rolsTotal);
    }

    /**
     * @param CreateTypeDocumentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/typeDocuments",
     *      summary="Store a newly created TypeDocument in storage",
     *      tags={"TypeDocument"},
     *      description="Store TypeDocument",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeDocument that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeDocument")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeDocument"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTypeDocumentAPIRequest $request)
    {
        $input = $request->all();

        $typeDocument = $this->typeDocumentRepository->create($input);

        return $this->sendResponse($typeDocument->toArray(), 'Type Document saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/typeDocuments/{id}",
     *      summary="Display the specified TypeDocument",
     *      tags={"TypeDocument"},
     *      description="Get TypeDocument",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeDocument",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeDocument"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TypeDocument $typeDocument */
        $typeDocument = $this->typeDocumentRepository->find($id);

        if (empty($typeDocument)) {
            return $this->sendError('Type Document not found');
        }

        return $this->sendResponse($typeDocument->toArray(), 'Type Document retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTypeDocumentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/typeDocuments/{id}",
     *      summary="Update the specified TypeDocument in storage",
     *      tags={"TypeDocument"},
     *      description="Update TypeDocument",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeDocument",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeDocument that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeDocument")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeDocument"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTypeDocumentAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeDocument $typeDocument */
        $typeDocument = $this->typeDocumentRepository->find($id);

        if (empty($typeDocument)) {
            return $this->sendError('Type Document not found');
        }

        $typeDocument = $this->typeDocumentRepository->update($input, $id);

        return $this->sendResponse($typeDocument->toArray(), 'TypeDocument updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/typeDocuments/{id}",
     *      summary="Remove the specified TypeDocument from storage",
     *      tags={"TypeDocument"},
     *      description="Delete TypeDocument",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeDocument",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TypeDocument $typeDocument */
        $typeDocument = $this->typeDocumentRepository->find($id);

        if (empty($typeDocument)) {
            return $this->sendError('Type Document not found');
        }

        $typeDocument->delete();

        return $this->sendSuccess('Type Document deleted successfully');
    }
}
