<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserRolAPIRequest;
use App\Http\Requests\API\UpdateUserRolAPIRequest;
use App\Models\UserRol;
use App\Repositories\UserRolRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class UserRolController
 * @package App\Http\Controllers\API
 */

class UserRolAPIController extends AppBaseController
{
    /** @var  UserRolRepository */
    private $userRolRepository;

    public function __construct(UserRolRepository $userRolRepo)
    {
        $this->userRolRepository = $userRolRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/userRols",
     *      summary="Get a listing of the UserRols.",
     *      tags={"UserRol"},
     *      description="Get all UserRols",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UserRol")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $userRols = $this->userRolRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($userRols->toArray(), 'User Rols retrieved successfully');
    }

    /**
     * @param CreateUserRolAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/userRols",
     *      summary="Store a newly created UserRol in storage",
     *      tags={"UserRol"},
     *      description="Store UserRol",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UserRol that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UserRol")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserRol"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUserRolAPIRequest $request)
    {
        $input = $request->all();

        $userRol = $this->userRolRepository->create($input);

        return $this->sendResponse($userRol->toArray(), 'User Rol saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/userRols/{id}",
     *      summary="Display the specified UserRol",
     *      tags={"UserRol"},
     *      description="Get UserRol",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserRol",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserRol"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var UserRol $userRol */
        $userRol = $this->userRolRepository->find($id);

        if (empty($userRol)) {
            return $this->sendError('User Rol not found');
        }

        return $this->sendResponse($userRol->toArray(), 'User Rol retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUserRolAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/userRols/{id}",
     *      summary="Update the specified UserRol in storage",
     *      tags={"UserRol"},
     *      description="Update UserRol",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserRol",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UserRol that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UserRol")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserRol"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUserRolAPIRequest $request)
    {
        $input = $request->all();

        /** @var UserRol $userRol */
        $userRol = $this->userRolRepository->find($id);

        if (empty($userRol)) {
            return $this->sendError('User Rol not found');
        }

        $userRol = $this->userRolRepository->update($input, $id);

        return $this->sendResponse($userRol->toArray(), 'UserRol updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/userRols/{id}",
     *      summary="Remove the specified UserRol from storage",
     *      tags={"UserRol"},
     *      description="Delete UserRol",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserRol",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var UserRol $userRol */
        $userRol = $this->userRolRepository->find($id);

        if (empty($userRol)) {
            return $this->sendError('User Rol not found');
        }

        $userRol->delete();

        return $this->sendSuccess('User Rol deleted successfully');
    }
}
