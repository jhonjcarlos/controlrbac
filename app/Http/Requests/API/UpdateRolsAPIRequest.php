<?php

namespace App\Http\Requests\API;

use App\Models\Rols;
use InfyOm\Generator\Request\APIRequest;

class UpdateRolsAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Rols::$rules;
        
        return $rules;
    }
}
