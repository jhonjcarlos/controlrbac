<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @SWG\Definition(
 *      definition="AccessDocument",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="document_id",
 *          description="document_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="access_id",
 *          description="access_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_rol_id",
 *          description="user_rol_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class AccessDocument extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'access_documents';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'document_id',
        'access_id',
        'user_rol_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'document_id' => 'integer',
        'access_id' => 'integer',
        'user_rol_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function userRol()
    {
        return $this->belongsTo(UserRol::class, 'user_rol_id');
    }

    public function document()
    {
        return $this->belongsTo(Documents::class, 'document_id');
    }

    public function access()
    {
        return $this->belongsTo(Access::class, 'access_id');
    }
}
