<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @SWG\Definition(
 *      definition="Chat",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          description="message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="file_chat",
 *          description="file_chat",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="user_to_id",
 *          description="user_to_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_from_id",
 *          description="user_from_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_group_id",
 *          description="user_group_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Chat extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'chats';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'message',
        'state',
        'file_chat',
        'user_to_id',
        'user_from_id',
        'user_group_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'message' => 'string',
        'state' => 'string',
        'file_chat' => 'string',
        'user_to_id' => 'integer',
        'user_from_id' => 'integer',
        'user_group_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function userTo()
    {
        return $this->belongsTo(User::class, 'user_to_id');
    }

    public function userFrom()
    {
        return $this->belongsTo(User::class, 'user_from_id');
    }

    public function userGroup()
    {
        return $this->belongsTo(UserGroup::class, 'group_user_id');
    }
}
