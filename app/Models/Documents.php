<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @SWG\Definition(
 *      definition="Documents",
 *      required={"name", "routeFile"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="detail",
 *          description="detail",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="routeFile",
 *          description="routeFile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="parentFile",
 *          description="parentFile",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type_doc_id",
 *          description="type_doc_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="access_id",
 *          description="access_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Documents extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'documents';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'detail',
        'routeFile',
        'parentFile',
        'user_id',
        'type_doc_id',
        'access_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'detail' => 'string',
        'routeFile' => 'string',
        'parentFile' => 'integer',
        'user_id' => 'integer',
        'type_doc_id' => 'integer',
        'access_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'routeFile' => 'required'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function typeDocument()
    {
        return $this->belongsTo(TypeDocument::class, 'type_doc_id');
    }

    public function access()
    {
        return $this->belongsTo(Access::class, 'access_id');
    }

    public function accessDocument()
    {
        return $this->hasMany(AccessDocument::class);
    }
}
