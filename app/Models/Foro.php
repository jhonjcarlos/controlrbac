<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @SWG\Definition(
 *      definition="Foro",
 *      required={"title"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="detail",
 *          description="detail",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="route_file",
 *          description="route_file",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="user_rol_id",
 *          description="user_rol_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Foro extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'foros';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'detail',
        'state',
        'route_file',
        'user_rol_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'detail' => 'string',
        'state' => 'boolean',
        'route_file' => 'string',
        'user_rol_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required'
    ];

    public function userRol()
    {
        return $this->belongsTo(UserRol::class, 'user_rol_id');
    }
}
