<?php

namespace App\Repositories;

use App\Models\AccessDocument;
use App\Repositories\BaseRepository;

/**
 * Class AccessDocumentRepository
 * @package App\Repositories
 * @version February 15, 2021, 12:01 am UTC
*/

class AccessDocumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'document_id',
        'access_id',
        'user_rol_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AccessDocument::class;
    }
}
