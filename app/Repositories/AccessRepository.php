<?php

namespace App\Repositories;

use App\Models\Access;
use App\Repositories\BaseRepository;

/**
 * Class AccessRepository
 * @package App\Repositories
 * @version January 28, 2021, 3:45 am UTC
*/

class AccessRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'detail'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Access::class;
    }
}
