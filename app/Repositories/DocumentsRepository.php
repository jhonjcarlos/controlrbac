<?php

namespace App\Repositories;

use App\Models\Documents;
use App\Repositories\BaseRepository;

/**
 * Class DocumentsRepository
 * @package App\Repositories
 * @version January 29, 2021, 4:14 am UTC
*/

class DocumentsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'detail',
        'routeFile',
        'parentFile',
        'user_id',
        'type_doc_id',
        'access_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Documents::class;
    }
}
