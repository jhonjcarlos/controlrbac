<?php

namespace App\Repositories;

use App\Models\Foro;
use App\Repositories\BaseRepository;

/**
 * Class ForoRepository
 * @package App\Repositories
 * @version February 15, 2021, 3:27 am UTC
*/

class ForoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'detail',
        'state',
        'route_file',
        'user_rol_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Foro::class;
    }
}
