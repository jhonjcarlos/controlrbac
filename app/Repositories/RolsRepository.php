<?php

namespace App\Repositories;

use App\Models\Rols;
use App\Repositories\BaseRepository;

/**
 * Class RolsRepository
 * @package App\Repositories
 * @version January 28, 2021, 3:23 am UTC
*/

class RolsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'detail'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rols::class;
    }
}
