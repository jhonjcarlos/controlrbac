<?php

namespace App\Repositories;

use App\Models\TypeDocument;
use App\Repositories\BaseRepository;

/**
 * Class TypeDocumentRepository
 * @package App\Repositories
 * @version January 27, 2021, 2:04 am UTC
*/

class TypeDocumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'detail'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeDocument::class;
    }
}
