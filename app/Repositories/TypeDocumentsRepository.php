<?php

namespace App\Repositories;

use App\Models\TypeDocuments;
use App\Repositories\BaseRepository;

/**
 * Class TypeDocumentsRepository
 * @package App\Repositories
 * @version January 28, 2021, 3:33 am UTC
*/

class TypeDocumentsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'detail'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeDocuments::class;
    }
}
