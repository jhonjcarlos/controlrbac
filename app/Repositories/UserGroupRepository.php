<?php

namespace App\Repositories;

use App\Models\UserGroup;
use App\Repositories\BaseRepository;

/**
 * Class UserGroupRepository
 * @package App\Repositories
 * @version January 29, 2021, 3:03 am UTC
*/

class UserGroupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'group_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserGroup::class;
    }
}
