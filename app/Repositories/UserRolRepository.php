<?php

namespace App\Repositories;

use App\Models\UserRol;
use App\Repositories\BaseRepository;

/**
 * Class UserRolRepository
 * @package App\Repositories
 * @version January 29, 2021, 4:38 am UTC
*/

class UserRolRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'rol_id',
        'group_user_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserRol::class;
    }
}
