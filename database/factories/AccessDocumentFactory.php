<?php

namespace Database\Factories;

use App\Models\AccessDocument;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccessDocumentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AccessDocument::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'document_id' => $this->faker->randomDigitNotNull,
        'access_id' => $this->faker->randomDigitNotNull,
        'user_rol_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
