<?php

namespace Database\Factories;

use App\Models\Foro;
use Illuminate\Database\Eloquent\Factories\Factory;

class ForoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Foro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
        'detail' => $this->faker->text,
        'state' => $this->faker->word,
        'route_file' => $this->faker->text,
        'user_rol_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
