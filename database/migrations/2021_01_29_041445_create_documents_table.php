<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('detail');
            $table->text('routeFile');
            $table->integer('parentFile');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('type_doc_id');
            $table->unsignedBigInteger('access_id');
            $table->timestamps();
            $table->softDeletes();
            /*$table->foreign('user_id')->references('id')->on('User');
            $table->foreign('type_doc_id')->references('id')->on('TypeDocument');
            $table->foreign('access_id')->references('id')->on('Access');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}
