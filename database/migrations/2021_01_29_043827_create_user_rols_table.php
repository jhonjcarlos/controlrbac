<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRolsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rols', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('rol_id');
            $table->unsignedBigInteger('group_user_id');
            $table->timestamps();
            $table->softDeletes();
            /*$table->foreign('user_id')->references('id')->on('User');
            $table->foreign('rol_id')->references('id')->on('Rols');
            $table->foreign('group_user_id')->references('id')->on('GroupUser');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_rols');
    }
}
