<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessDocumentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_id')->unsigned();
            $table->integer('access_id')->unsigned();
            $table->integer('user_rol_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            /*$table->foreign('document_id')->references('id')->on('documents');
            $table->foreign('access_id')->references('id')->on('accesses');
            $table->foreign('user_rol_id')->references('id')->on('user_rols');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('access_documents');
    }
}
