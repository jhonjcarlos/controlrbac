<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->string('state');
            $table->text('file_chat');
            $table->integer('user_to_id')->unsigned();
            $table->integer('user_from_id')->unsigned();
            $table->integer('user_group_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            /*$table->foreign('user_to_id')->references('id')->on('users');
            $table->foreign('user_from_id')->references('id')->on('users');
            $table->foreign('user_group_id')->references('id')->on('users');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chats');
    }
}
