<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RolsAPIController;
use App\Http\Controllers\API\TypeDocumentAPIController;
use App\Http\Controllers\API\AccessAPIController;
use App\Http\Controllers\API\GroupAPIController;
use App\Http\Controllers\API\UserGroupAPIController;
use App\Http\Controllers\API\DocumentsAPIController;
use App\Http\Controllers\API\UserRolAPIController;
use App\Http\Controllers\API\AccessDocumentAPIController;
use App\Http\Controllers\API\ForoAPIController;
use App\Http\Controllers\API\ChatAPIController;
use App\Http\Controllers\API\UserAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::resource('rols', [RolsAPIController::class]);
Route::get('v1/rols', [RolsAPIController::class, 'index']);
Route::post('v1/rols', [RolsAPIController::class, 'store']);
Route::get('v1/rols/{id}', [RolsAPIController::class, 'show']);
Route::put('v1/rols/{id}', [RolsAPIController::class, 'update']);
Route::delete('v1/rols/{id}', [RolsAPIController::class, 'destroy']);

// Route::resource('type_documents', [TypeDocumentAPIController::class);
Route::get('v1/typeDocuments', [TypeDocumentAPIController::class, 'index']);
Route::post('v1/typeDocuments', [TypeDocumentAPIController::class, 'store']);
Route::get('v1/typeDocuments/{id}', [TypeDocumentAPIController::class, 'show']);
Route::put('v1/typeDocuments/{id}', [TypeDocumentAPIController::class, 'update']);
Route::delete('v1/typeDocuments/{id}', [TypeDocumentAPIController::class, 'destroy']);

// Route::resource('accesses', [AccessAPIController::class]);
Route::get('v1/accesses', [AccessAPIController::class, 'index']);
Route::post('v1/accesses', [AccessAPIController::class, 'store']);
Route::get('v1/accesses/{id}', [AccessAPIController::class, 'show']);
Route::put('v1/accesses/{id}', [AccessAPIController::class, 'update']);
Route::delete('v1/accesses/{id}', [AccessAPIController::class, 'destroy']);

// Route::resource('groups', [GroupAPIController::class]);
Route::get('v1/groups', [GroupAPIController::class, 'index']);
Route::post('v1/groups', [GroupAPIController::class, 'store']);
Route::get('v1/groups/{id}', [GroupAPIController::class, 'show']);
Route::put('v1/groups/{id}', [GroupAPIController::class, 'update']);
Route::delete('v1/groups/{id}', [GroupAPIController::class, 'destroy']);


// Route::resource('user_groups', [UserGroupAPIController::class]);
Route::get('v1/userGroups', [UserGroupAPIController::class, 'index']);
Route::post('v1/userGroups', [UserGroupAPIController::class, 'store']);
Route::get('v1/userGroups/{id}', [UserGroupAPIController::class, 'show']);
Route::put('v1/userGroups/{id}', [UserGroupAPIController::class, 'update']);
Route::delete('v1/userGroups/{id}', [UserGroupAPIController::class, 'destroy']);

//Route::resource('v1/documents', DocumentsAPIController::class);
Route::get('v1/documents', [DocumentsAPIController::class, 'index']);
Route::post('v1/documents', [DocumentsAPIController::class, 'store']);
Route::get('v1/documents/{id}', [DocumentsAPIController::class, 'show']);
Route::put('v1/documents/{id}', [DocumentsAPIController::class, 'update']);
Route::delete('v1/documents/{id}', [DocumentsAPIController::class, 'destroy']);

// Route::resource('user_rols', UserRolAPIController::class);
Route::get('v1/userRols', [UserRolAPIController::class, 'index']);
Route::post('v1/userRols', [UserRolAPIController::class, 'store']);
Route::get('v1/userRols/{id}', [UserRolAPIController::class, 'show']);
Route::put('v1/userRols/{id}', [UserRolAPIController::class, 'update']);
Route::delete('v1/userRols/{id}', [UserRolAPIController::class, 'destroy']);

// Route::resource('access_documents', AccessDocumentAPIController::class);
Route::get('v1/accessDocuments', [AccessDocumentAPIController::class, 'index']);
Route::post('v1/accessDocuments', [AccessDocumentAPIController::class, 'store']);
Route::get('v1/accessDocuments/{id}', [AccessDocumentAPIController::class, 'show']);
Route::put('v1/accessDocuments/{id}', [AccessDocumentAPIController::class, 'update']);
Route::delete('v1/accessDocuments/{id}', [AccessDocumentAPIController::class, 'destroy']);

//Route::resource('foros', App\Http\Controllers\API\ForoAPIController::class);
Route::get('v1/foros', [ForoAPIController::class, 'index']);
Route::post('v1/foros', [ForoAPIController::class, 'store']);
Route::get('v1/foros/{id}', [ForoAPIController::class, 'show']);
Route::put('v1/foros/{id}', [ForoAPIController::class, 'update']);
Route::delete('v1/foros/{id}', [ForoAPIController::class, 'destroy']);


// Route::resource('chats', [ChatAPIController::class]);
Route::get('v1/chats', [ChatAPIController::class, 'index']);
Route::post('v1/chats', [ChatAPIController::class, 'store']);
Route::get('v1/chats/{id}', [ChatAPIController::class, 'show']);
Route::put('v1/chats/{id}', [ChatAPIController::class, 'update']);
Route::delete('v1/chats/{id}', [ChatAPIController::class, 'destroy']);

// Route::resource('users', [ChatAPIController::class]);
Route::get('v1/users', [UserAPIController::class, 'index']);
Route::post('v1/users', [UserAPIController::class, 'store']);
Route::get('v1/users/{id}', [UserAPIController::class, 'show']);
Route::put('v1/users/{id}', [UserAPIController::class, 'update']);
Route::delete('v1/users/{id}', [UserAPIController::class, 'destroy']);
