<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Access;

class AccessApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_access()
    {
        $access = Access::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/accesses', $access
        );

        $this->assertApiResponse($access);
    }

    /**
     * @test
     */
    public function test_read_access()
    {
        $access = Access::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/accesses/'.$access->id
        );

        $this->assertApiResponse($access->toArray());
    }

    /**
     * @test
     */
    public function test_update_access()
    {
        $access = Access::factory()->create();
        $editedAccess = Access::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/accesses/'.$access->id,
            $editedAccess
        );

        $this->assertApiResponse($editedAccess);
    }

    /**
     * @test
     */
    public function test_delete_access()
    {
        $access = Access::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/accesses/'.$access->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/accesses/'.$access->id
        );

        $this->response->assertStatus(404);
    }
}
