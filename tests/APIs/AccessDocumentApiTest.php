<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\AccessDocument;

class AccessDocumentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_access_document()
    {
        $accessDocument = AccessDocument::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/access_documents', $accessDocument
        );

        $this->assertApiResponse($accessDocument);
    }

    /**
     * @test
     */
    public function test_read_access_document()
    {
        $accessDocument = AccessDocument::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/access_documents/'.$accessDocument->id
        );

        $this->assertApiResponse($accessDocument->toArray());
    }

    /**
     * @test
     */
    public function test_update_access_document()
    {
        $accessDocument = AccessDocument::factory()->create();
        $editedAccessDocument = AccessDocument::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/access_documents/'.$accessDocument->id,
            $editedAccessDocument
        );

        $this->assertApiResponse($editedAccessDocument);
    }

    /**
     * @test
     */
    public function test_delete_access_document()
    {
        $accessDocument = AccessDocument::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/access_documents/'.$accessDocument->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/access_documents/'.$accessDocument->id
        );

        $this->response->assertStatus(404);
    }
}
