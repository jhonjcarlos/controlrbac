<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Documents;

class DocumentsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_documents()
    {
        $documents = Documents::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/documents', $documents
        );

        $this->assertApiResponse($documents);
    }

    /**
     * @test
     */
    public function test_read_documents()
    {
        $documents = Documents::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/documents/'.$documents->id
        );

        $this->assertApiResponse($documents->toArray());
    }

    /**
     * @test
     */
    public function test_update_documents()
    {
        $documents = Documents::factory()->create();
        $editedDocuments = Documents::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/documents/'.$documents->id,
            $editedDocuments
        );

        $this->assertApiResponse($editedDocuments);
    }

    /**
     * @test
     */
    public function test_delete_documents()
    {
        $documents = Documents::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/documents/'.$documents->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/documents/'.$documents->id
        );

        $this->response->assertStatus(404);
    }
}
