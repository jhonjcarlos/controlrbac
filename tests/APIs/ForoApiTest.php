<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Foro;

class ForoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_foro()
    {
        $foro = Foro::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/foros', $foro
        );

        $this->assertApiResponse($foro);
    }

    /**
     * @test
     */
    public function test_read_foro()
    {
        $foro = Foro::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/foros/'.$foro->id
        );

        $this->assertApiResponse($foro->toArray());
    }

    /**
     * @test
     */
    public function test_update_foro()
    {
        $foro = Foro::factory()->create();
        $editedForo = Foro::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/foros/'.$foro->id,
            $editedForo
        );

        $this->assertApiResponse($editedForo);
    }

    /**
     * @test
     */
    public function test_delete_foro()
    {
        $foro = Foro::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/foros/'.$foro->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/foros/'.$foro->id
        );

        $this->response->assertStatus(404);
    }
}
