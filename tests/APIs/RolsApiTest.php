<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Rols;

class RolsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_rols()
    {
        $rols = Rols::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/rols', $rols
        );

        $this->assertApiResponse($rols);
    }

    /**
     * @test
     */
    public function test_read_rols()
    {
        $rols = Rols::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/rols/'.$rols->id
        );

        $this->assertApiResponse($rols->toArray());
    }

    /**
     * @test
     */
    public function test_update_rols()
    {
        $rols = Rols::factory()->create();
        $editedRols = Rols::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/rols/'.$rols->id,
            $editedRols
        );

        $this->assertApiResponse($editedRols);
    }

    /**
     * @test
     */
    public function test_delete_rols()
    {
        $rols = Rols::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/rols/'.$rols->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/rols/'.$rols->id
        );

        $this->response->assertStatus(404);
    }
}
