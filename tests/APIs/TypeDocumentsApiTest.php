<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TypeDocuments;

class TypeDocumentsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_type_documents()
    {
        $typeDocuments = TypeDocuments::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/type_documents', $typeDocuments
        );

        $this->assertApiResponse($typeDocuments);
    }

    /**
     * @test
     */
    public function test_read_type_documents()
    {
        $typeDocuments = TypeDocuments::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/type_documents/'.$typeDocuments->id
        );

        $this->assertApiResponse($typeDocuments->toArray());
    }

    /**
     * @test
     */
    public function test_update_type_documents()
    {
        $typeDocuments = TypeDocuments::factory()->create();
        $editedTypeDocuments = TypeDocuments::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/type_documents/'.$typeDocuments->id,
            $editedTypeDocuments
        );

        $this->assertApiResponse($editedTypeDocuments);
    }

    /**
     * @test
     */
    public function test_delete_type_documents()
    {
        $typeDocuments = TypeDocuments::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/type_documents/'.$typeDocuments->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/type_documents/'.$typeDocuments->id
        );

        $this->response->assertStatus(404);
    }
}
