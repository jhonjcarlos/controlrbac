<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\UserRol;

class UserRolApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_user_rol()
    {
        $userRol = UserRol::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/user_rols', $userRol
        );

        $this->assertApiResponse($userRol);
    }

    /**
     * @test
     */
    public function test_read_user_rol()
    {
        $userRol = UserRol::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/user_rols/'.$userRol->id
        );

        $this->assertApiResponse($userRol->toArray());
    }

    /**
     * @test
     */
    public function test_update_user_rol()
    {
        $userRol = UserRol::factory()->create();
        $editedUserRol = UserRol::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/user_rols/'.$userRol->id,
            $editedUserRol
        );

        $this->assertApiResponse($editedUserRol);
    }

    /**
     * @test
     */
    public function test_delete_user_rol()
    {
        $userRol = UserRol::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/user_rols/'.$userRol->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/user_rols/'.$userRol->id
        );

        $this->response->assertStatus(404);
    }
}
