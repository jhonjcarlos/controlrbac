<?php namespace Tests\Repositories;

use App\Models\AccessDocument;
use App\Repositories\AccessDocumentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AccessDocumentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AccessDocumentRepository
     */
    protected $accessDocumentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->accessDocumentRepo = \App::make(AccessDocumentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_access_document()
    {
        $accessDocument = AccessDocument::factory()->make()->toArray();

        $createdAccessDocument = $this->accessDocumentRepo->create($accessDocument);

        $createdAccessDocument = $createdAccessDocument->toArray();
        $this->assertArrayHasKey('id', $createdAccessDocument);
        $this->assertNotNull($createdAccessDocument['id'], 'Created AccessDocument must have id specified');
        $this->assertNotNull(AccessDocument::find($createdAccessDocument['id']), 'AccessDocument with given id must be in DB');
        $this->assertModelData($accessDocument, $createdAccessDocument);
    }

    /**
     * @test read
     */
    public function test_read_access_document()
    {
        $accessDocument = AccessDocument::factory()->create();

        $dbAccessDocument = $this->accessDocumentRepo->find($accessDocument->id);

        $dbAccessDocument = $dbAccessDocument->toArray();
        $this->assertModelData($accessDocument->toArray(), $dbAccessDocument);
    }

    /**
     * @test update
     */
    public function test_update_access_document()
    {
        $accessDocument = AccessDocument::factory()->create();
        $fakeAccessDocument = AccessDocument::factory()->make()->toArray();

        $updatedAccessDocument = $this->accessDocumentRepo->update($fakeAccessDocument, $accessDocument->id);

        $this->assertModelData($fakeAccessDocument, $updatedAccessDocument->toArray());
        $dbAccessDocument = $this->accessDocumentRepo->find($accessDocument->id);
        $this->assertModelData($fakeAccessDocument, $dbAccessDocument->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_access_document()
    {
        $accessDocument = AccessDocument::factory()->create();

        $resp = $this->accessDocumentRepo->delete($accessDocument->id);

        $this->assertTrue($resp);
        $this->assertNull(AccessDocument::find($accessDocument->id), 'AccessDocument should not exist in DB');
    }
}
