<?php namespace Tests\Repositories;

use App\Models\Access;
use App\Repositories\AccessRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AccessRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AccessRepository
     */
    protected $accessRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->accessRepo = \App::make(AccessRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_access()
    {
        $access = Access::factory()->make()->toArray();

        $createdAccess = $this->accessRepo->create($access);

        $createdAccess = $createdAccess->toArray();
        $this->assertArrayHasKey('id', $createdAccess);
        $this->assertNotNull($createdAccess['id'], 'Created Access must have id specified');
        $this->assertNotNull(Access::find($createdAccess['id']), 'Access with given id must be in DB');
        $this->assertModelData($access, $createdAccess);
    }

    /**
     * @test read
     */
    public function test_read_access()
    {
        $access = Access::factory()->create();

        $dbAccess = $this->accessRepo->find($access->id);

        $dbAccess = $dbAccess->toArray();
        $this->assertModelData($access->toArray(), $dbAccess);
    }

    /**
     * @test update
     */
    public function test_update_access()
    {
        $access = Access::factory()->create();
        $fakeAccess = Access::factory()->make()->toArray();

        $updatedAccess = $this->accessRepo->update($fakeAccess, $access->id);

        $this->assertModelData($fakeAccess, $updatedAccess->toArray());
        $dbAccess = $this->accessRepo->find($access->id);
        $this->assertModelData($fakeAccess, $dbAccess->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_access()
    {
        $access = Access::factory()->create();

        $resp = $this->accessRepo->delete($access->id);

        $this->assertTrue($resp);
        $this->assertNull(Access::find($access->id), 'Access should not exist in DB');
    }
}
