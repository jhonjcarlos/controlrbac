<?php namespace Tests\Repositories;

use App\Models\Documents;
use App\Repositories\DocumentsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DocumentsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DocumentsRepository
     */
    protected $documentsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->documentsRepo = \App::make(DocumentsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_documents()
    {
        $documents = Documents::factory()->make()->toArray();

        $createdDocuments = $this->documentsRepo->create($documents);

        $createdDocuments = $createdDocuments->toArray();
        $this->assertArrayHasKey('id', $createdDocuments);
        $this->assertNotNull($createdDocuments['id'], 'Created Documents must have id specified');
        $this->assertNotNull(Documents::find($createdDocuments['id']), 'Documents with given id must be in DB');
        $this->assertModelData($documents, $createdDocuments);
    }

    /**
     * @test read
     */
    public function test_read_documents()
    {
        $documents = Documents::factory()->create();

        $dbDocuments = $this->documentsRepo->find($documents->id);

        $dbDocuments = $dbDocuments->toArray();
        $this->assertModelData($documents->toArray(), $dbDocuments);
    }

    /**
     * @test update
     */
    public function test_update_documents()
    {
        $documents = Documents::factory()->create();
        $fakeDocuments = Documents::factory()->make()->toArray();

        $updatedDocuments = $this->documentsRepo->update($fakeDocuments, $documents->id);

        $this->assertModelData($fakeDocuments, $updatedDocuments->toArray());
        $dbDocuments = $this->documentsRepo->find($documents->id);
        $this->assertModelData($fakeDocuments, $dbDocuments->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_documents()
    {
        $documents = Documents::factory()->create();

        $resp = $this->documentsRepo->delete($documents->id);

        $this->assertTrue($resp);
        $this->assertNull(Documents::find($documents->id), 'Documents should not exist in DB');
    }
}
