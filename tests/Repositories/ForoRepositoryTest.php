<?php namespace Tests\Repositories;

use App\Models\Foro;
use App\Repositories\ForoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ForoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ForoRepository
     */
    protected $foroRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->foroRepo = \App::make(ForoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_foro()
    {
        $foro = Foro::factory()->make()->toArray();

        $createdForo = $this->foroRepo->create($foro);

        $createdForo = $createdForo->toArray();
        $this->assertArrayHasKey('id', $createdForo);
        $this->assertNotNull($createdForo['id'], 'Created Foro must have id specified');
        $this->assertNotNull(Foro::find($createdForo['id']), 'Foro with given id must be in DB');
        $this->assertModelData($foro, $createdForo);
    }

    /**
     * @test read
     */
    public function test_read_foro()
    {
        $foro = Foro::factory()->create();

        $dbForo = $this->foroRepo->find($foro->id);

        $dbForo = $dbForo->toArray();
        $this->assertModelData($foro->toArray(), $dbForo);
    }

    /**
     * @test update
     */
    public function test_update_foro()
    {
        $foro = Foro::factory()->create();
        $fakeForo = Foro::factory()->make()->toArray();

        $updatedForo = $this->foroRepo->update($fakeForo, $foro->id);

        $this->assertModelData($fakeForo, $updatedForo->toArray());
        $dbForo = $this->foroRepo->find($foro->id);
        $this->assertModelData($fakeForo, $dbForo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_foro()
    {
        $foro = Foro::factory()->create();

        $resp = $this->foroRepo->delete($foro->id);

        $this->assertTrue($resp);
        $this->assertNull(Foro::find($foro->id), 'Foro should not exist in DB');
    }
}
