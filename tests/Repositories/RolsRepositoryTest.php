<?php namespace Tests\Repositories;

use App\Models\Rols;
use App\Repositories\RolsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RolsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RolsRepository
     */
    protected $rolsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->rolsRepo = \App::make(RolsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_rols()
    {
        $rols = Rols::factory()->make()->toArray();

        $createdRols = $this->rolsRepo->create($rols);

        $createdRols = $createdRols->toArray();
        $this->assertArrayHasKey('id', $createdRols);
        $this->assertNotNull($createdRols['id'], 'Created Rols must have id specified');
        $this->assertNotNull(Rols::find($createdRols['id']), 'Rols with given id must be in DB');
        $this->assertModelData($rols, $createdRols);
    }

    /**
     * @test read
     */
    public function test_read_rols()
    {
        $rols = Rols::factory()->create();

        $dbRols = $this->rolsRepo->find($rols->id);

        $dbRols = $dbRols->toArray();
        $this->assertModelData($rols->toArray(), $dbRols);
    }

    /**
     * @test update
     */
    public function test_update_rols()
    {
        $rols = Rols::factory()->create();
        $fakeRols = Rols::factory()->make()->toArray();

        $updatedRols = $this->rolsRepo->update($fakeRols, $rols->id);

        $this->assertModelData($fakeRols, $updatedRols->toArray());
        $dbRols = $this->rolsRepo->find($rols->id);
        $this->assertModelData($fakeRols, $dbRols->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_rols()
    {
        $rols = Rols::factory()->create();

        $resp = $this->rolsRepo->delete($rols->id);

        $this->assertTrue($resp);
        $this->assertNull(Rols::find($rols->id), 'Rols should not exist in DB');
    }
}
