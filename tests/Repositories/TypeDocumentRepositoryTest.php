<?php namespace Tests\Repositories;

use App\Models\TypeDocument;
use App\Repositories\TypeDocumentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TypeDocumentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeDocumentRepository
     */
    protected $typeDocumentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->typeDocumentRepo = \App::make(TypeDocumentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_type_document()
    {
        $typeDocument = TypeDocument::factory()->make()->toArray();

        $createdTypeDocument = $this->typeDocumentRepo->create($typeDocument);

        $createdTypeDocument = $createdTypeDocument->toArray();
        $this->assertArrayHasKey('id', $createdTypeDocument);
        $this->assertNotNull($createdTypeDocument['id'], 'Created TypeDocument must have id specified');
        $this->assertNotNull(TypeDocument::find($createdTypeDocument['id']), 'TypeDocument with given id must be in DB');
        $this->assertModelData($typeDocument, $createdTypeDocument);
    }

    /**
     * @test read
     */
    public function test_read_type_document()
    {
        $typeDocument = TypeDocument::factory()->create();

        $dbTypeDocument = $this->typeDocumentRepo->find($typeDocument->id);

        $dbTypeDocument = $dbTypeDocument->toArray();
        $this->assertModelData($typeDocument->toArray(), $dbTypeDocument);
    }

    /**
     * @test update
     */
    public function test_update_type_document()
    {
        $typeDocument = TypeDocument::factory()->create();
        $fakeTypeDocument = TypeDocument::factory()->make()->toArray();

        $updatedTypeDocument = $this->typeDocumentRepo->update($fakeTypeDocument, $typeDocument->id);

        $this->assertModelData($fakeTypeDocument, $updatedTypeDocument->toArray());
        $dbTypeDocument = $this->typeDocumentRepo->find($typeDocument->id);
        $this->assertModelData($fakeTypeDocument, $dbTypeDocument->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_type_document()
    {
        $typeDocument = TypeDocument::factory()->create();

        $resp = $this->typeDocumentRepo->delete($typeDocument->id);

        $this->assertTrue($resp);
        $this->assertNull(TypeDocument::find($typeDocument->id), 'TypeDocument should not exist in DB');
    }
}
