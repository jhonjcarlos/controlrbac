<?php namespace Tests\Repositories;

use App\Models\TypeDocuments;
use App\Repositories\TypeDocumentsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TypeDocumentsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeDocumentsRepository
     */
    protected $typeDocumentsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->typeDocumentsRepo = \App::make(TypeDocumentsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_type_documents()
    {
        $typeDocuments = TypeDocuments::factory()->make()->toArray();

        $createdTypeDocuments = $this->typeDocumentsRepo->create($typeDocuments);

        $createdTypeDocuments = $createdTypeDocuments->toArray();
        $this->assertArrayHasKey('id', $createdTypeDocuments);
        $this->assertNotNull($createdTypeDocuments['id'], 'Created TypeDocuments must have id specified');
        $this->assertNotNull(TypeDocuments::find($createdTypeDocuments['id']), 'TypeDocuments with given id must be in DB');
        $this->assertModelData($typeDocuments, $createdTypeDocuments);
    }

    /**
     * @test read
     */
    public function test_read_type_documents()
    {
        $typeDocuments = TypeDocuments::factory()->create();

        $dbTypeDocuments = $this->typeDocumentsRepo->find($typeDocuments->id);

        $dbTypeDocuments = $dbTypeDocuments->toArray();
        $this->assertModelData($typeDocuments->toArray(), $dbTypeDocuments);
    }

    /**
     * @test update
     */
    public function test_update_type_documents()
    {
        $typeDocuments = TypeDocuments::factory()->create();
        $fakeTypeDocuments = TypeDocuments::factory()->make()->toArray();

        $updatedTypeDocuments = $this->typeDocumentsRepo->update($fakeTypeDocuments, $typeDocuments->id);

        $this->assertModelData($fakeTypeDocuments, $updatedTypeDocuments->toArray());
        $dbTypeDocuments = $this->typeDocumentsRepo->find($typeDocuments->id);
        $this->assertModelData($fakeTypeDocuments, $dbTypeDocuments->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_type_documents()
    {
        $typeDocuments = TypeDocuments::factory()->create();

        $resp = $this->typeDocumentsRepo->delete($typeDocuments->id);

        $this->assertTrue($resp);
        $this->assertNull(TypeDocuments::find($typeDocuments->id), 'TypeDocuments should not exist in DB');
    }
}
