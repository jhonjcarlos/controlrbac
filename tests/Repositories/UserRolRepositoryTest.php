<?php namespace Tests\Repositories;

use App\Models\UserRol;
use App\Repositories\UserRolRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class UserRolRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserRolRepository
     */
    protected $userRolRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->userRolRepo = \App::make(UserRolRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_user_rol()
    {
        $userRol = UserRol::factory()->make()->toArray();

        $createdUserRol = $this->userRolRepo->create($userRol);

        $createdUserRol = $createdUserRol->toArray();
        $this->assertArrayHasKey('id', $createdUserRol);
        $this->assertNotNull($createdUserRol['id'], 'Created UserRol must have id specified');
        $this->assertNotNull(UserRol::find($createdUserRol['id']), 'UserRol with given id must be in DB');
        $this->assertModelData($userRol, $createdUserRol);
    }

    /**
     * @test read
     */
    public function test_read_user_rol()
    {
        $userRol = UserRol::factory()->create();

        $dbUserRol = $this->userRolRepo->find($userRol->id);

        $dbUserRol = $dbUserRol->toArray();
        $this->assertModelData($userRol->toArray(), $dbUserRol);
    }

    /**
     * @test update
     */
    public function test_update_user_rol()
    {
        $userRol = UserRol::factory()->create();
        $fakeUserRol = UserRol::factory()->make()->toArray();

        $updatedUserRol = $this->userRolRepo->update($fakeUserRol, $userRol->id);

        $this->assertModelData($fakeUserRol, $updatedUserRol->toArray());
        $dbUserRol = $this->userRolRepo->find($userRol->id);
        $this->assertModelData($fakeUserRol, $dbUserRol->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_user_rol()
    {
        $userRol = UserRol::factory()->create();

        $resp = $this->userRolRepo->delete($userRol->id);

        $this->assertTrue($resp);
        $this->assertNull(UserRol::find($userRol->id), 'UserRol should not exist in DB');
    }
}
